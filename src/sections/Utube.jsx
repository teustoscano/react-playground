import React from 'react'
import "../styles/utube.scss"

import ReactPlayer from 'react-player'
import { io } from 'socket.io-client'

const NEW_VIDEO_URI_EVENT = "new-video-uri"
const PLAY_PAUSE_EVENT = "play-pause"
const SOCKET_SERVER_URI = "http://localhost:4001"

export default function Utube() {
    const [videoURI, setVideoURI] = React.useState("")
    const [videoStatus, setVideoStatus] = React.useState(false)

    let uriRef = React.createRef()
    let socketRef = React.createRef()

    React.useEffect(() => {

        socketRef.current = io(SOCKET_SERVER_URI)

        socketRef.current.on("connect", data => {
            socketRef.current.on("hello", args => {
                console.log(args)
            })
        })

        socketRef.current.on('video-from-server', args => {
            setVideoURI(args)
        })

        socketRef.current.on('play-from-server', args => {
            console.log('ARROCHA')
            setVideoStatus(args)
        })

        return () => socketRef.current.disconnect()
    }, [socketRef])

    const sendVideoToBackEnd = (uri) => {
        if(!socketRef.current) return ;
        socketRef.current.emit(NEW_VIDEO_URI_EVENT, uri)
    }

    const playPauseVideo = (status) => {
        if(!socketRef.current) return;
        setVideoStatus(status)
        socketRef.current.emit(PLAY_PAUSE_EVENT, status)
    }

    console.log('URI ',videoStatus)
    return (
        <section className="u-wrapper">
            <div className="u-container">
                {/* <div className="u-title">
                    <h1>WETUBE <span className="u-by">by </span><span>Domenica.</span></h1>
                </div> */}
                <div className="u-video">
                    <ReactPlayer
                        url={videoURI}
                        onPause={() => playPauseVideo(false)}
                        onPlay={() => playPauseVideo(true)}
                        playing={videoStatus}
                    />
                </div>
                <div className="u-search">
                    <input
                        ref={uriRef}
                        type="text"
                        placeholder="Youtube Video URL"
                    />
                    <button onClick={() => sendVideoToBackEnd(uriRef.current.value)}>SEND</button>                    
                </div>
                {/* <div className="u-container-chat">
                    <p>msg</p>
                </div> */}
            </div>
        </section>
    )
}
