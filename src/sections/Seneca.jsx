import React from 'react'
import "../styles/seneca.scss"
import { FaInstagram, FaLinkedinIn, FaFacebookSquare } from 'react-icons/fa'

import Logo from '../images/logoRedondo.png'
import Hero from '../images/domenica4.jpeg'

function Seneca() {
    return (
        <section className="s-wrapper">
            <div className="s-conatiner">
                <div className="s-header">
                    <div className="s-header-img">
                        <img src={Logo} />
                    </div>
                    <div className="s-header-links">
                        <p>Who</p>
                        <p>Cases</p>
                        <p>Contact</p>
                    </div>
                </div>
                <div className="s-hero">
                    <img src={Hero} />
                    <div className="s-hero-text">
                        <h1>
                            Domenica.
                    </h1>
                        <p>
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat sed.
                    </p>
                    </div>
                </div>
                <div className="s-links">
                    <li><FaInstagram /></li>
                    <li><FaLinkedinIn /></li>
                    <li><FaFacebookSquare /></li>
                </div>
                <button>Get in touch</button>
            </div>
        </section>
    )
}

export default Seneca
