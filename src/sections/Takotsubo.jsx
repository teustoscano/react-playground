import React from 'react'
import "../styles/takotsubo.scss"
import { FaInstagram, FaLinkedinIn, FaFacebookSquare } from 'react-icons/fa'

import Hero from '../images/hero1.png'
import Logo from '../images/logo.png'

function Takotsubo() {
    return (
        <section className="t-wrapper">
            <div className="t-logo">
                <img src={Logo} alt="Logo"/>
            </div>
            <div className="t-bg"/>
            <div className="t-container">
                <div className="t-links">
                    <li><FaInstagram /></li>
                    <li><FaLinkedinIn /></li>
                    <li><FaFacebookSquare /></li>
                </div>
                <div className="t-image">
                    <img src={Hero} alt=""/>
                </div>
                <h1>DOMENICA.</h1>
            </div>
        </section>
    )
}

export default Takotsubo
