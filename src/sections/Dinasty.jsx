import React from 'react'
import "../styles/dinasty.scss"

import Logo from '../images/logoRedondoPreto.png'
import Hero from '../images/domenica5.jpeg'

function Dinasty() {
    return (
        <section className="d-wrapper">
            <div className="d-container">
                <div className="d-image">
                    <img src={Logo} alt=""/>
                </div>

                <div className="d-hero">
                    <img src={Hero} alt=""/>
                </div>

                <div className="d-title">
                    <h1>We are<br/>Domenica.</h1>
                    <p>
                        We believe in sharing<br/>Unique brand with the world<br/>Your ideia in her prime
                    </p>
                </div>

                <div className="d-call">
                    <h1>And we do!</h1>
                </div>
            </div>
        </section>
    )
}

export default Dinasty
