import React from 'react'
import "../styles/apura.scss"

import { ParallaxProvider, Parallax } from 'react-scroll-parallax';


import Hero1 from '../images/domenica5.jpeg'
import Hero2 from '../images/domenica4.jpeg'


function Apura() {

    const [offsetY, setOffsetY] = React.useState(0)
    const handleScroll = () => setOffsetY(window.pageYOffset)

    React.useEffect(() => {
        window.addEventListener('scroll', handleScroll)

        return () => window.removeEventListener('scroll', handleScroll)
    }, [])

    return (
        <section className="a-wrapper">
            <div className="a-container">
                <div className="a-title">
                    <h1>APURA</h1>
                </div>
                <div
                    className="a-parallax-wrapper"
                >
                    <div
                        className="a-parallax"
                        style={{ transform: `translateY(${offsetY * .5}px)` }}
                    >
                        <img src={Hero1} alt="" />
                        <h2>CAESAR</h2>
                    </div>
                    <div
                        className="a-parallax"
                        style={{ transform: `translateY(${offsetY * .5}px)` }}
                    >
                        <img src={Hero2} alt="" />
                        <h2>ROME</h2>
                    </div>
                    <div
                        className="a-parallax"
                        style={{ transform: `translateY(${offsetY * .35}px)` }}
                    >
                        <img src={Hero2} alt="" />
                        <h2>PRINCIPIS VLS</h2>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Apura
